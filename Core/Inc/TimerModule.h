/*
Library:			Timer Module
Written by:			Ronald Bazillion
Date written:			19/10/2018
Description:			This is the timer header file for STM32 MCU	
Resources:

Modifications:
    12/31/2018                  Ronald Bazillion                Added Comments for each API function
                                                                Added TmrIsStarted and TmrIsEnabled API functions
-----------------

*/

#ifndef __TIMER_MODULE_H_
#define __TIMER_MODULE_H_

//Header files
#include "stm32l4xx_hal.h"
#include <stdbool.h>

//*******Defines***************//

#define TMR_MAX_SIZE 16
#define UNSUCCESSFUL 0xFF

//*******Enums*****************//


//*******Flags****************//


//***** Constant variables and typedefs *****//
typedef void (*TMR_FUNCT_PTR)(uint8_t);

//*******Structures****************//

typedef struct tmr
{
  bool tmrEnable;
  bool tmrStart;
  bool oneTime;
  uint8_t tmrId;
  uint16_t tmrCounter;
  uint16_t tmrMaxValue;
  TMR_FUNCT_PTR tmrFnctPtr;
  uint8_t arg;
} TMR;

//***** Global Variables ************//


//***** API Functions prototype *****//

/*Timer initialization 
  ARG: *htim - pointer to the timer resource 
  Return: none
*/
void TmrInit(TIM_HandleTypeDef *htim);

/*Timer create - Used to create a timerId
  ARG: None
  Return: timerId if successful else UNSUCCESSFUL if not.
*/
uint8_t TmrCreate();

/* Timer populate used to configure the timer 
  ARG: tmrId - timer Id
  ARG: tmrMaxValue - value used for timeout
  ARG: tmrFnctPtr - function pointer used for callback when timer expires
  ARG: arg - argument used for function pointer
  ARG: onetime - boolean argument to determine if timer is recurring or one time.
       NOTE: if one time timer will not delete or reset but just stop. It's up the user
             to reset, restart, disable, or delete the timer. Once deleted it will be removed.
  Return: true is successful and false if not.
*/    
bool TmrPopulate(uint8_t tmrId, uint16_t tmrMaxValue, TMR_FUNCT_PTR tmrFnctPtr, uint8_t arg, bool oneTime);

/* Timer start 
   ARG: timerId - timer Id for specific timer
   Return: true is successful and false if not. 
*/
bool TmrStart( uint8_t timerId );

/* Timer Stop 
   ARG: timerId - timer Id for specific timer
   Return: true is successful and false if not.
*/
bool TmrStop( uint8_t timerId );

/* Timer Reset 
   ARG: timerId - timer Id for specific timer
   Return: true is successful and false if not.
*/
bool TmrReset( uint8_t timerId );

/* Timer Delete 
   ARG: timerId - timer Id for specific timer
   Return: true is successful and false if not.
*/
bool TmrDelete( uint8_t timerId );

/* Timer process array of timers 
   ARG: *htim - pointer to the timer resource 
   Return: none
*/
void TmrProcessTimerArray(TIM_HandleTypeDef *htim);

/* Timer is timer started
   ARG: timerId - Id for timer
   Return: True if started, False is not
*/
bool TmrIsStarted( uint8_t timerId );

/* Timer is timer enabled
   ARG: timerId - Id for timer
   Return: True if enabled, False is not
*/
bool TmrIsEnabled( uint8_t timerId );

/* Timer Create - used to create an interruptable timer.
   ARG: value - 16 bit value representing the timeout used
   ARG: fPtr - function pointer used to house the function. Must be of type TMR_FUNCT_PTR
   ARG: oneTime -  boolean value used to dtermine is recurring or one time. TRUE is One time, FALSE if recurring
   Return: timerID

   NOTE: for all timers they must be manually started with TmrStart(timerID);
*/
uint8_t TmrCreateTimer(uint16_t value, TMR_FUNCT_PTR fPtr, bool oneTime);

/* Timer change timer value - used to change the timer value based upon id.
   ARG: timerId - Id for timer
   ARG: value - 16 bit value representing the timeout used
   Return: True if changed, False is not

   NOTE: Timer must be enabled and stopped for this function to work
         This function will reset the timer counter
*/
bool TmrChangeTimerValue(uint8_t timerId, uint16_t value);

#endif
