/*
Library:			Timer Module for STM32 MCUs
Written by:			Ronald Bazillion
Date written:			19/10/2018
Description:			This is the timer implementation file using the STM32 MCU
                                	
Resources:

Modifications:
    12/31/2018                  Ronald Bazillion        In TimerReset changed tmrEnable to true
                                                        In TmrProcessTimerArray commented out m_DeleteTimer and moved 
                                                        reset of counter only if a recurring timer.
                                                        Added TmrIsStarted and TmrIsEnabled API functions
-----------------

*/

//***** Header files *****//
#include "TimerModule.h"

//***** typedefs enums *****//


//***** typedefs structs *****//


//***** static Library variables *****//
//****Remember "static" is used like "private" in C++*******//
static TMR m_TmrModuleArray[TMR_MAX_SIZE];
static TIM_HandleTypeDef *m_Htim;

//*********static function prototypes***********************//
//****Remember "static" is used like "private" in C++*******//
bool m_StopTimer( uint8_t timerId );
bool m_DeleteTimer( uint8_t timerId );

//***** API Functions definition *****//
//****Remember "API" is used like "public" in C++*******//

void TmrInit(TIM_HandleTypeDef *htim)
{
    m_Htim = htim;

    for (int i=0; i<TMR_MAX_SIZE; i++)
    {
      m_TmrModuleArray[i].tmrEnable = false;
      m_TmrModuleArray[i].tmrStart = false;
      m_TmrModuleArray[i].oneTime = false;
      m_TmrModuleArray[i].tmrId = 0;
      m_TmrModuleArray[i].tmrCounter = 0;
      m_TmrModuleArray[i].tmrMaxValue = 0;
      m_TmrModuleArray[i].tmrFnctPtr = NULL;
      m_TmrModuleArray[i].arg = 0;
    }
}

uint8_t TmrCreate()
{
  int i = UNSUCCESSFUL;
  
  for (i=0; i<TMR_MAX_SIZE; i++)
  {
    if (m_TmrModuleArray[i].tmrEnable == false)
    {
       m_TmrModuleArray[i].tmrEnable = true;
       break;
    }
  }
  
  return i;
}

bool TmrPopulate(uint8_t tmrId, uint16_t tmrMaxValue, TMR_FUNCT_PTR tmrFnctPtr, uint8_t arg, bool oneTime)
{ 
  if ((tmrId > TMR_MAX_SIZE) || (m_TmrModuleArray[tmrId].tmrEnable == false))
  {
    return false;
  }
  
  m_TmrModuleArray[tmrId].tmrStart = false;
  m_TmrModuleArray[tmrId].tmrCounter = 0;
  
  m_TmrModuleArray[tmrId].tmrId = tmrId;
  m_TmrModuleArray[tmrId].tmrMaxValue = tmrMaxValue;
  m_TmrModuleArray[tmrId].tmrFnctPtr = tmrFnctPtr;
  m_TmrModuleArray[tmrId].arg = arg;
  m_TmrModuleArray[tmrId].oneTime = oneTime;
  
  return true;  
}

bool TmrStart( uint8_t timerId )
{
  
  if ((m_TmrModuleArray[timerId].tmrEnable == false) ||
     (m_TmrModuleArray[timerId].tmrStart == true) || 
     (timerId >=  TMR_MAX_SIZE))
  {
    return false;
  }
  
  m_TmrModuleArray[timerId].tmrStart = true;
  return true;
}

bool TmrStop( uint8_t timerId )
{
  return m_StopTimer(timerId);
}

bool TmrReset( uint8_t timerId )
{
  if ((m_TmrModuleArray[timerId].tmrEnable == false) ||
     (m_TmrModuleArray[timerId].tmrStart == true) || 
     (timerId >=  TMR_MAX_SIZE))
  {
    return false;
  }
  
  m_TmrModuleArray[timerId].tmrCounter = 0;
  return true;
}

bool TmrDelete( uint8_t timerId )
{
  return m_DeleteTimer(timerId);
}              

void TmrProcessTimerArray(TIM_HandleTypeDef *htim)
{
  int i;
  
  if (htim->Instance == m_Htim->Instance)
  {
      for (i=0; i<TMR_MAX_SIZE; i++)
      {
        if (m_TmrModuleArray[i].tmrEnable == true)
        {
          if (m_TmrModuleArray[i].tmrStart == true)
          {
            if (m_TmrModuleArray[i].tmrCounter >= m_TmrModuleArray[i].tmrMaxValue)
            {
                m_TmrModuleArray[i].tmrFnctPtr(m_TmrModuleArray[i].tmrId);
                
                if (m_TmrModuleArray[i].oneTime == true)
                {
                  m_StopTimer(i);
                }
                else
                {
                  m_TmrModuleArray[i].tmrCounter = 0;
                }
            }
            else
            {
                m_TmrModuleArray[i].tmrCounter++;
            }
          }   
        }
      }
  }  
}

bool TmrIsStarted( uint8_t timerId )
{
    if (timerId > TMR_MAX_SIZE-1)
      return false;
    
    if (m_TmrModuleArray[timerId].tmrStart == true)
    {
      return true;
    }
    
    return false;
}

bool TmrIsEnabled( uint8_t timerId )
{
    if (timerId > TMR_MAX_SIZE-1)
      return false;
    
    if (m_TmrModuleArray[timerId].tmrEnable == true)
    {
      return true;
    }
    
    return false;
}

uint8_t TmrCreateTimer(uint16_t value, TMR_FUNCT_PTR fPtr, bool oneTime)
{
    uint8_t timerId  = TmrCreate();
    if (timerId != UNSUCCESSFUL)
    {
      if (TmrPopulate(timerId, value, fPtr, 0, oneTime))
      {
          TmrReset(timerId);
      }
      else
      {
        TmrDelete(timerId);
        return UNSUCCESSFUL;
      }
    }
    
    return timerId;
}

bool TmrChangeTimerValue(uint8_t timerId, uint16_t value)
{
    if ((m_TmrModuleArray[timerId].tmrEnable == false) ||       //if not enabled or
     (m_TmrModuleArray[timerId].tmrStart == true) ||            //if started or
     (timerId >=  TMR_MAX_SIZE))                                //if greater than max timer value
    {
        return false;
    }

    m_TmrModuleArray[timerId].tmrCounter = 0;
    m_TmrModuleArray[timerId].tmrMaxValue = value;

    return true;
}

//************************************************** STATIC FUNCTIONS ***********************************************
//*******************************************************************************************************************
//*******************************************************************************************************************
bool m_StopTimer( uint8_t timerId )
{
    if ((m_TmrModuleArray[timerId].tmrEnable == false) ||
     (m_TmrModuleArray[timerId].tmrStart == false) ||
     (timerId >=  TMR_MAX_SIZE))
  {
    return false;
  }
  
  m_TmrModuleArray[timerId].tmrStart = false;
  return true;
}

bool m_DeleteTimer( uint8_t timerId )
{
    if ((m_TmrModuleArray[timerId].tmrEnable == false) ||
     (m_TmrModuleArray[timerId].tmrStart == true) || 
     (timerId >=  TMR_MAX_SIZE))
  {
    return false;
  }
  
  m_TmrModuleArray[timerId].tmrEnable = false;
  m_TmrModuleArray[timerId].tmrStart = false;
  m_TmrModuleArray[timerId].oneTime = false;
  m_TmrModuleArray[timerId].tmrId = 0;
  m_TmrModuleArray[timerId].tmrCounter = 0;
  m_TmrModuleArray[timerId].tmrMaxValue = 0;
  m_TmrModuleArray[timerId].tmrFnctPtr = NULL;
  return true;
}
